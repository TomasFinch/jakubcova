<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>

    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/bootstrap-datepicker.css">
</head>

<body>

    <div class="container-fluid p-0">
        <nav class="navbar navbar-expand-lg navbar-dark bg-primary fixed-top">
            <a href="#" class="navbar-brand">Statek Chotoviny</a>

            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target='#navbarSupportedContent'>
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a href="#" class="nav-link">Domů</a>
                    </li>
                    <li class="nav-item">
                        <a href="#" class="nav-link">Galerie</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a href="#" class="nav-link">Rezervace</a>
                    </li>
                    <li class="nav-item">
                        <a href="#" class="nav-link">Kontakt</a>
                    </li>
                </ul>
            </div>
        </nav>

        <div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#carouselExampleCaptions" data-slide-to="0" class="active"></li>
                <li data-target="#carouselExampleCaptions" data-slide-to="1"></li>
                <li data-target="#carouselExampleCaptions" data-slide-to="2"></li>
                <li data-target="#carouselExampleCaptions" data-slide-to="3"></li>
            </ol>
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img src="img/carousel-1.jpg" class="d-block w-100" alt="...">
                    <div class="carousel-caption d-none d-md-block ">
                        <h5>Můžete si pronajmout náš úžasný statek</h5>
                        <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
                    </div>
                </div>
                <div class="carousel-item">
                    <img src="img/carousel-2.jpg" class="d-block w-100" alt="...">
                    <div class="carousel-caption d-none d-md-block ">
                        <h5>Můžete si pronajmout náš úžasný statek</h5>
                        <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
                    </div>
                </div>
                <div class="carousel-item">
                    <img src="img/carousel-3.jpg" class="d-block w-100" alt="...">
                    <div class="carousel-caption d-none d-md-block ">
                        <h5>Můžete si pronajmout náš úžasný statek</h5>
                        <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
                    </div>
                </div>
                <div class="carousel-item">
                    <img src="img/carousel-4.jpg" class="d-block w-100" alt="...">
                    <div class="carousel-caption d-none d-md-block ">
                        <h5>Můžete si pronajmout náš úžasný statek</h5>
                        <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
                    </div>
                </div>
            </div>
            <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
        <div class="container">



            <div class="row d-flex justify-content-center">
                <form name='emailForm' class="mt-4 col-sm-12 col-md-8" action='email.php' method='post'>
                    <div class="row">
                        <div class="col-sm-12 col-md-7">
                            <div class="form-group col-sm-12">
                                <label for="inputName4">Jméno</label>
                                <input type="text" name="name" class="form-control" id="inputName4" placeholder="Jméno">
                            </div>
                            <div class="form-group col-sm-12">
                                <label for="inputEmail4">Email</label>
                                <input type="email" name="email" class="form-control" id="inputEmail4" placeholder="Email">
                            </div>
                            <div class="form-group col-sm-12">
                                <label for="inputAddress">Adresa</label>
                                <input type="text" name="adress" class="form-control" id="inputAddress" placeholder="Adresa">
                            </div>
                            <div class="row col-sm-12 m-0 p-0">
                                <div class="form-group col-sm-6 datepicker">
                                    <label for="datepicker">Od</label>
                                    <input type='text' name="startDate" class="form-control " data-date-language='cs' data-provide="startDate" id='startDate' value="Od kdy">
                                </div>
                                <div class="form-group col-sm-6 datepicker">
                                    <label for="datepicker">Do</label>
                                    <input type='text' name="endDate" class="form-control " data-date-language='cs' data-provide="endDate" id='endDate' value="Do kdy">
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-sm-12 col-md-5">
                            <label for="inputCity">Napište nám</label>
                            <textarea name="message" class="form-control" id="message" cols="25" rows="12"></textarea>
                        </div>
                    </div>
                    <!-- <div class='form-group'>
                        <label for="datepicker">Od</label>
                        <input type='text' class="form-control col-sm-6" data-date-language='cs'
                            data-provide="startDate" id='datepicker' value="Od kdy">
                        <label for="datepicker">Do</label>
                        <input type='text' class="form-control col-sm-6" data-date-language='cs' data-provide="endDate"
                            value="Do kdy">
                    </div> -->

                    <button type="submit" name="submit" class="btn btn-primary btn-center mb-4">Odeslat</button>
                </form>




            </div>

            <div class="jumbotron">
                <h1 class="display-4">Simple something.</h1>
                <p class="lead">Lorem ipsum dolor sit amet consectetur adipisicing elit. Est tempore praesentium iusto
                    vitae
                    veritatis, error natus numquam eos, eaque, iste vero deleniti quidem doloribus. Eaque necessitatibus
                    facilis voluptatum corrupti perferendis!</p>
                <p class="lead">
                    <a href="" class="btn btn-primary btn-lg">Watch me</a>
                </p>
            </div>
            <div class="row">
                <div class="col-sm-12 col-md-4">
                    <div class="card mb-4">
                        <div class="card-body text-center">
                            <h5 class="card-title">Card title</h5>
                            <p class="card-text">some text kadg;kjngadnv a hafvnlk</p>
                            <a href="#" class="card-link"></a>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="card">
                        <div class="card-body text-center">
                            <h5 class="card-title">Card title</h5>
                            <p class="card-text">some text kadg;kjngadnv a hafvnlk</p>
                            <a href="#" class="card-link"></a>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="card">
                        <div class="card-body text-center">
                            <h5 class="card-title">Card title</h5>
                            <p class="card-text">some text kadg;kjngadnv a hafvnlk</p>
                            <a href="#" class="card-link"></a>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>


    <script src="js/jquery.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/bootstrap-datepicker.min.js"></script>
    <script src="js/bootstrap-datepicker.cs.min.js"></script>
    <script src="js/script.js"></script>
</body>

</html>